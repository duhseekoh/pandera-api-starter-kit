
exports.seed = (knex, Promise) =>
  // Deletes ALL existing entries
  knex('Articles')
    .del()
    .then(() => Promise.all([
      // Inserts seed entries
      knex('Articles').insert({
        id: 1,
        title: 'Foo',
        content: 'Quis tempus blandit in in, a montes viverra, non condimentum dui sed egestas imperdiet, maecenas tortor ac, pharetra adipiscing felis. Blandit in arcu, orci sollicitudin amet in erat, libero accusamus mauris quisque, sem proin nulla neque erat, volutpat wisi vitae nec etiam vitae aliquam. Nunc tempus vel egestas, venenatis duis massa augue ut orci pulvinar, ullamcorper adipiscing non, est mattis nec vivamus neque suspendisse quis.',
      }),
    ])
  );
