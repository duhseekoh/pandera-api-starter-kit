module.exports = {
  local: {
    client: 'sqlite3',
    connection: {
      filename: './local.sqlite',
    },
  },
  development: {
    client: 'pg',
    connection: process.env.DATABASE_URL,
  },
  staging: {
    client: 'pg',
    connection: process.env.DATABASE_URL,
  },
  production: {
    client: 'pg',
    connection: process.env.DATABASE_URL,
  },
};
