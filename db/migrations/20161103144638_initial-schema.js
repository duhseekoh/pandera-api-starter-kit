
exports.up = function up(knex) {
  return knex.schema.createTable('Articles', (table) => {
    table.increments('id').primary();
    table.string('title');
    table.text('content');
  });
};

exports.down = function down(knex) {
  knex.schema.dropTableIfExists('Article');
};
