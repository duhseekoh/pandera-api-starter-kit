FROM node:6.9.0
COPY ./build /src
RUN cd /src && npm install
EXPOSE 3001
WORKDIR /src
CMD ["npm", "start"]
