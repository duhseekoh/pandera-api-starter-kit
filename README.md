# Overview
This is an API application starter that can be forked/reused to build lightweight, simple, database-backed APIs. It uses the following:

* Koa 2: http://koajs.com/
* Webpack: https://webpack.github.io/


## Quickstart
1. Fork the repo on Github

1. Git clone the forked project locally
  ```shell
  git clone {repo}
  ```

1. Install software dependencies
  - Node v6 (http://nodejs.org | homebrew)
  - Postresql if running in development/staging/prod (postgres)

1. Install Package dependencies
  ```shell
  npm i
  ```

1. Install knex cli for migrations/seeds
  ```shell
  npm i -g knex
  ```

1. Migrate your database
  ```shell
  # Use env local to set up a local sqlite database
  npm run db:migrate -- --env local
  ```

1. Seed the data
  ```shell
  # Use env local to set up a local sqlite database
  npm run db:seed -- --env local
  ```


1. Start building your api/hooking up your dependencies
