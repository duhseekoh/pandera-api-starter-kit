import Promise from 'bluebird';
import fs from 'fs';
import pkg from '../package.json';

/**
 * Copies static files such as robots.txt, favicon.ico to the
 * output (build) folder.
 */
async function copy() {
  /* eslint-disable global-require */
  const ncp = Promise.promisify(require('ncp'));

  const writeFilep = Promise.promisify(fs.writeFile);
  const mkdirp = Promise.promisify(fs.mkdir);
  const statp = Promise.promisify(fs.stat);

  try {
    const result = await statp('build');
    console.log(result);
  } catch (e) {
    mkdirp('build');
  }
  console.log('Inside copy command, going to copy folders'); // eslint-disable-line no-console
  await Promise.all([
    ncp('package.json', 'build/package.json'),
    writeFilep('build/package.json', JSON.stringify({
      private: true,
      engines: pkg.engines,
      dependencies: pkg.dependencies,
      scripts: {
        start: 'node server.bundle.js',
      },
    })),
    ncp('db', 'build/db'),
  ]);

  console.log('finished copying folders'); // eslint-disable-line no-console
}

export default copy;
