const path = require('path');
const fs = require('fs');
const nodeExternals = require('webpack-node-externals');

const nodeModules = {};
fs.readdirSync('node_modules')
  .filter(x => ['.bin'].indexOf(x) === -1)
  .map(module => // eslint-disable-line no-return-assign
    nodeModules[module] = `commonjs ${module}`
  );

module.exports = {
  devtool: 'eval',
  entry: [
    'babel-polyfill',
    './server.js',
  ],
  output: {
    path: path.resolve(__dirname, '../build'),
    filename: 'server.bundle.js',
    libraryTarget: 'commonjs2',
  },
  context: path.resolve(__dirname, '../src'),
  target: 'node',
  externals: [nodeExternals()],
  resolve: {
    extensions: ['', '.webpack.js', '.js', '.jsx', '.json'],
    alias: {
      'any-promise': 'bluebird',
    },
  },
  module: {
    loaders: [
      {
        // Only run `.js` and `.jsx` files through Babel
        test: /\.jsx?$/,
        loader: 'babel',
        query: {
          presets: ['es2015', 'es2017'],
        },
      },
      {
        test: /\.jsx?$/,
        loader: 'eslint-loader',
        options: {
          emitWarning: true,
          
        },
      },
      {
        test: /\.json$/,
        loader: 'json-loader',
      },
    ],
  },
  exclude: nodeModules,
};
