import {} from 'dotenv/config';
import webpack from 'webpack';
import path from 'path';
import cp from 'child_process';
import ProgressBarPlugin from 'progress-bar-webpack-plugin';
import webpackConfig from './webpack.config';
import copy from './copy';

let server = null;
const { output } = webpackConfig;
const serverPath = path.join(output.path, output.filename);

function runServer() {
  if (server) {
    server.kill('SIGTERM');
  }

  server = cp.spawn('node', [serverPath], {
    env: Object.assign({ NODE_ENV: 'development' }, process.env),
    silent: false,
  });

  server.stdout.on('data', x => process.stdout.write(x));
  server.stderr.on('data', x => process.stderr.write(x));
}


async function watch() {
  await copy();
  const compiler = webpack(webpackConfig);
  compiler.apply(new ProgressBarPlugin());
  compiler.watch({

  }, (err, stats) => {
    if (err) {
      server.kill('SIGTERM');
    } else {
      console.log('restarting server');
      runServer();
    }

    const jsonStats = stats.toJson();
    if (jsonStats.errors.length > 0) {
      jsonStats.errors.forEach((error) => {
        console.log(error);
      });
    }
    if (jsonStats.warnings.length > 0) {
      jsonStats.warnings.forEach((warning) => {
        console.log(warning);
      });
    }
  });
}

watch();
