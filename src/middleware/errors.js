/* @flow */
import type { KoaContext } from 'koa-flow-declarations/KoaContext';

export default () =>
  async (ctx: KoaContext, next: () => Promise<any>) => {
    try {
      await next();
    } catch (err) {
      ctx.status = err.status || 500;
      ctx.body = err.message;
      ctx.app.emit('error', err, this);
      ctx.foo = ctx.method * 2;
    }
  };
