/* @flow */
/* eslint-disable class-methods-use-this */
/*
  Writing services as classes to support injecting other services
  When ready, create a constructor that accepts an opts object,
  then deconstruct any included items and store them to the instance (this)
*/
import Article from '../models/Article';

export default class ArticleService {

  async getAll(): Promise<any> {
    const articles = await Article
      .query()
      .limit(10);

    return articles;
  }

  async getArticleById(id: number): Promise<any> {
    if (!id) throw new Error('No id for article');
    const article = await Article
      .query()
      .where('id', id)
      .first();
    if (!article) throw new Error('Article not found');
    return article;
  }
}
