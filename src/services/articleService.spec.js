import test from 'tape';
import knex from 'knex';
import Chance from 'chance';
import mockDb from 'mock-knex';
import Article from '../models/Article';
import ArticleService from './articleService';

const db = knex({
  client: 'sqlite3',
});

const tracker = mockDb.getTracker();
tracker.install();

const chance = new Chance();

mockDb.mock(db);
Article.knex(db);

const articleService = new ArticleService();

test('Example test', (t) => {
  t.ok(true);
  t.end();
});

test('ArticleService', async (t) => {
  const article = {
    title: chance.word(),
    content: chance.paragraph(),
  };
  tracker.on('query', (query) => {
    query.response([article]);
  });

  const articles = await articleService.getAll();
  t.ok(articles[0], 'Should return an article');
  t.equals(articles[0].title, article.title);
  t.equals(articles[0].content, article.content);
  t.end();
});
