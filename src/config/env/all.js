export default {
  db: {
    client: 'pg',
    connection: process.env.DATABASE_URL,
  },
};
