export default {
  db: {
    client: 'sqlite3',
    connection: {
      filename: './db/local.sqlite',
    },
  },
};
