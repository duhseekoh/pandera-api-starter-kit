import _ from 'lodash';

// The code below may seem weird, but it allows all of the
// configs to be bundled via webpack and not
// dynamically required
import all from './env/all';
import local from './env/local';
import development from './env/development';
import staging from './env/staging';
import production from './env/production';

const env = process.env.NODE_ENV || 'development';
/* eslint-disable global-require */
const configs = {
  all,
  local,
  development,
  staging,
  production,
};
/* eslint-enable global-require */

const config = _.merge({}, configs.all, configs[env]);
export default config;
