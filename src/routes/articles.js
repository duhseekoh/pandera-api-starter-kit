import Router from 'koa-router';

const router = new Router();

router.get('/', async (ctx) => {
  const { articleService } = ctx.services;
  ctx.type = 'application/json';
  ctx.status = 200;
  ctx.body = await articleService.getAll();
});

router.get('/:id', async (ctx) => {
  const { id } = ctx.params;
  const { articleService } = ctx.services;
  const article = await articleService.getArticleById(id);
  if (!article) {
    ctx.status = 404;
    return;
  }
  ctx.type = 'application/json';
  ctx.status = 200;
  ctx.body = article;
});

export default router;
