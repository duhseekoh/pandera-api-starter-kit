import Router from 'koa-router';
import articles from './articles';

const router = new Router();

router.get('/', (ctx) => {
  ctx.status = 200;
  ctx.body = 'Hello World';
});
router.use('/articles', articles.routes(), articles.allowedMethods());

export default router;
