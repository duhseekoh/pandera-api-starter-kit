/* @flow */
import { Model } from 'objection';

export default class Article extends Model {
  static get tableName(): string {
    return 'Articles';
  }

  static get jsonSchema(): string {
    return {
      type: 'object',
      properties: {
        id: { type: 'integer' },
        title: { type: 'string', minLength: 1, maxLength: 255 },
        content: { type: 'text' },
      },
    };
  }
}
