import Koa from 'koa';
import json from 'koa-json';
import koalogger from 'koa-logger';
import convert from 'koa-convert';
import bodyParser from 'koa-bodyparser';

import Knex from 'knex';
import { Model } from 'objection';

import config from './config';
import routes from './routes';
import ArticleService from './services/articleService';
import errorHandler from './middleware/errors';

const knex = Knex(config.db); // eslint-disable-line new-cap
Model.knex(knex);

const port = process.env.PORT || 3001;
const app = new Koa();

// Instantiate any services passing in any necessary params
const services = {
  articleService: new ArticleService({ config }),
};

app.use(errorHandler());
app.use(convert(json()));
app.use(convert(koalogger()));
app.use(convert(bodyParser()));

app.use(async (ctx, next) => {
  // Apply services to the ctx so we can use them from routes
  Object.assign(ctx, { services });
  return await next();
});

app.use(routes.routes(), routes.allowedMethods());

app.listen(port, (err) => {
  if (err) throw err;
  console.log('The server is listening on port', port); // eslint-disable-line no-console
});
